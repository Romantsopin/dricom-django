from api.models import User, License, Token, Avatar, Channel, Message
from rest_framework import serializers


class LicenseSerializer(serializers.ModelSerializer):
    class Meta:
        model = License
        fields = ('id', 'title')

class AvatarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Avatar
        fields = ('image',)

class UserSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    licenses = LicenseSerializer(many=True, read_only=True)
    avatar = AvatarSerializer(read_only=True)
    phone = serializers.CharField(source='username')
    email = serializers.EmailField(required=False)
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)

    def create(self, validated_data):
        return User(**validated_data)

class UserPreviewSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    avatar = AvatarSerializer(read_only=True)
    phone = serializers.CharField(source='username')
    email = serializers.EmailField(required=False)
    first_name = serializers.CharField(required=False)

class TokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = ('id', 'value')

class MessageSerializer(serializers.ModelSerializer):
    owner = UserPreviewSerializer()
    class Meta:
        model = Message
        fields = ('id', 'text', 'owner', 'created_at')


class ChannelPreviewSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)
    user = serializers.SerializerMethodField()
    collocutor = serializers.SerializerMethodField()
    last_message = MessageSerializer()

    def get_user(self, channel):
        user = self.context.get('user')
        if not user:
            return None
        return UserPreviewSerializer(
            channel.members.filter(id=user.id).first()
        ).data

    def get_collocutor(self, channel):
        user = self.context.get('user')
        if not user:
            return None
        return UserPreviewSerializer(
            channel.members.exclude(id=user.id).first()
        ).data
