from django.shortcuts import render, get_object_or_404
from api.models import User, Token, License
from rest_framework import viewsets, status, mixins, generics
from rest_framework.response import Response
from api.serializers import UserSerializer, TokenSerializer, LicenseSerializer, AvatarSerializer, ChannelPreviewSerializer, MessageSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.decorators import api_view, permission_classes, parser_classes, list_route, detail_route
from rest_framework_jwt.settings import api_settings
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser

def gen_token(user):
    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return token

def jwt_response_payload_handler(token, user=None, request=None):
    return {
        'token': token,
        'user': UserSerializer(user, context={'request': request}).data
    }


@api_view(['GET', 'POST'])
def account(request):
    user = request.user
    if request.method == 'GET':
        serializer = UserSerializer(user)
        return Response(serializer.data, status=status.HTTP_200_OK)
    elif request.method == 'POST':
        if 'first_name' in request.data:
            user.first_name = request.data['first_name']
        if 'last_name' in request.data:
            user.last_name = request.data['last_name']
        if 'email' in request.data:
            email = request.data['email']
            if User.objects.filter(email=email).exclude(id=user.id).exists():
                return Response({'details': 'email is already used'}, status.HTTP_400_BAD_REQUEST)
            user.email = email
        if 'phone' in request.data:
            phone = request.data['phone']
            if User.objects.filter(username=phone).exclude(id=user.id).exists():
                return Response({'details': 'phone is already used'}, status.HTTP_400_BAD_REQUEST)
            user.username = phone
        user.save()
        return Response(
            jwt_response_payload_handler(gen_token(user), user=user, request=request)
        )

@api_view(['POST'])
def change_password(request):
    user = request.user
    old_password = request.data.get('old_password')
    new_password = request.data.get('new_password')
    if not old_password or not new_password:
        return Response({'details': 'bad request'}, status=status.HTTP_400_BAD_REQUEST)
    if not user.check_password(old_password):
        return Response({'details': 'wrong credentials'}, status=status.HTTP_403_FORBIDDEN)
    user.set_password(new_password)
    user.save()
    return Response({'details': 'success'})


@api_view(['POST'])
@permission_classes([AllowAny])
def register(request):
    phone = request.data.get('phone')
    password = request.data.get('password')
    token = request.data.get('token')
    license = request.data.get('license')

    if not phone or not password:
        return Response({'details': 'phone or password aren\'t provided'}, status=status.HTTP_400_BAD_REQUEST)
    if User.objects.filter(username=phone).exists():
        return Response({'details': 'phone is already registered'}, status=status.HTTP_400_BAD_REQUEST)
    if License.objects.filter(title=license).exists():
        return Response({'details': 'license is already registered'}, status=status.HTTP_400_BAD_REQUEST)
    if Token.objects.filter(value=token).exists():
        return Response({'details': 'token is already registered'}, status=status.HTTP_400_BAD_REQUEST)

    serializer = UserSerializer(data=request.data)
    if not serializer.is_valid():
        return Response({'details': serializer.errors})
    user = serializer.save()
    user.set_password(password)
    user.save()

    if license:
        user_license = License(title=license, user=user)
        user_license.save()
    if token:
        user_token = Token(value=token, user=user)
        user_token.save()

    return Response(
        jwt_response_payload_handler(gen_token(user), user=user, request=request)
    )


@api_view(['POST'])
@parser_classes((MultiPartParser, FormParser, FileUploadParser))
def upload_avatar(request):
    user = request.user
    if hasattr(user, 'avatar') and user.avatar:
        user.avatar.delete()
    if 'file' in request.data:
        request.data['image'] = request.data['file']
    serializer = AvatarSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save(user=user)
        return Response(
            jwt_response_payload_handler(gen_token(user), user=user, request=request)
        )
    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FavoriteUsersViewSet(mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    serializer_class = UserSerializer
    def get_queryset(self):
        if 'search' not in self.request.query_params:
            return self.request.user.favorite_users.all()

        search = self.request.query_params['search']
        return self.request.user.favorite_users.all().filter(licenses__title__startswith=search).distinct()[0:10]

    def create(self, request):
        user = self.request.user
        favorite = get_object_or_404(
            User,
            pk=request.data.get('user_id')
        )
        user.favorite_users.add(favorite)
        user.save()
        return Response({'detail': 'added'}, status=status.HTTP_201_CREATED)

    def destroy(self, request, pk=None):
        user = self.request.user
        favorite = get_object_or_404(User, pk=pk)
        user.favorite_users.remove(favorite)
        user.save()
        return Response({'detail': 'deleted'}, status=status.HTTP_200_OK)

class SearchUsers(generics.ListAPIView):
    serializer_class = UserSerializer
    def get_queryset(self):
        if 'license' not in self.request.query_params:
            return []
        startswith = self.request.query_params['license']
        queryset = User.objects.filter(is_superuser=False)
        return queryset.filter(licenses__title__startswith=startswith).distinct()[0:10]


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.filter(is_superuser=False)
    serializer_class = UserSerializer

class TokenViewSet(mixins.CreateModelMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    """
    API endpoint that allows tokens to be viewed or created.
    """
    queryset = Token.objects.all()
    serializer_class = TokenSerializer

    def create(self, request):
        serializer = TokenSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChannelViewSet(mixins.ListModelMixin,
                    mixins.RetrieveModelMixin,
                   viewsets.GenericViewSet):
    serializer_class = ChannelPreviewSerializer
    def get_queryset(self):
        return self.request.user.channel_set.all().order_by('-updated_at')
    def get_serializer(self, *args, **kwargs):
        cls = self.get_serializer_class()
        kwargs['context'] = {'user': self.request.user}
        return cls(*args, **kwargs)

    def create(self, request):
        user_id = request.data.get('user_id')
        if not user_id:
            return Response({'details': 'user_id not provided'}, status=status.HTTP_400_BAD_REQUEST)
        user = User.objects.get(pk=user_id)
        if not user:
            return Response({'details': 'bad user_id provided'}, status=status.HTTP_400_BAD_REQUEST)
        channel = request.user.maybe_create_channel_with_user(user)

        serializer = ChannelPreviewSerializer(channel, context={'user': request.user})
        return Response(serializer.data, status=status.HTTP_201_CREATED)


    @detail_route(methods=['get', 'post'])
    def messages(self, request, pk):
        if request.method == 'GET':
            channel = request.user.channel_set.get(pk=pk)
            messages = channel.message_set.all()

            page = self.paginate_queryset(messages)
            if page is not None:
                serializer = MessageSerializer(page[::-1], many=True)
                return self.get_paginated_response(serializer.data)

            serializer = self.MessageSerializer(messages.order_by('created_at'), many=True)
            return Response(serializer.data)
        elif request.method == 'POST':
            text = request.data.get('text')
            if not text:
                return Response({'detail': 'text not provided'}, status=status.HTTP_400_BAD_REQUEST)

            channel = request.user.channel_set.get(pk=pk)
            request.user.send_message_to_channel(text, channel)
            return Response({'detail': 'ok'})





class LicenseViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows licenses to be viewed or edited.
    """
    queryset = License.objects.all()
    serializer_class = LicenseSerializer

    def create(self, request):
        serializer = LicenseSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk):
        licenses = request.user.licenses.filter(pk=pk)
        if len(licenses) == 1:
            licenses[0].delete()
            return Response(\
                {'detail': 'License removed'},
                status=status.HTTP_200_OK
            )
        else:
            return Response(\
                {'detail': 'License not found'},
                status=status.HTTP_400_BAD_REQUEST
            )
