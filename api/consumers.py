from channels import Group
from channels.sessions import channel_session, enforce_ordering
from channels.auth import channel_session_user, channel_session_user_from_http
from api.models import User, Channel, Message
from rest_framework.renderers import JSONRenderer
from redis import StrictRedis
import json

r = StrictRedis()

def subscribe_user_to_chat_updates(user, reply):
    for channel in user.channel_set.all():
        Group('channel-%s-listener' % channel.id).add(reply)

def unsubscribe_user_from_chat_updates(user, reply):
    for channel in user.channel_set.all():
        Group('channel-%s-listener' % channel.id).discard(reply)


@channel_session_user_from_http
def ws_add(message):
    message.reply_channel.send({"accept": True})
    user = message.user
    if not user:
        return
    subscribe_user_to_chat_updates(user, message.reply_channel)
    message.reply_channel.send({
        "text": "ok"
    })

def deliver_message_to_channel(channel, user, message):
    # if not ack send -> go for notification
    Group('channel-%s-listener' % channel.id).send({
        'text': json.dumps({'channel': channel.id, 'user': user.id, 'message': message})
    })

@channel_session_user
def ws_message(message):
    command = json.loads(message.content['text'])
    user = message.user
    action = command.get('action')
    if action == 'send_message':
        chat_message = command.get('message')
        channel = Channel.objects.filter(
            pk=command.get('channel')
        ).first()

        if not channel or not chat_message:
            return
        user.send_message_to_channel(chat_message, channel)
        deliver_message_to_channel(channel, user, chat_message)

@channel_session_user
def ws_disconnect(message):
    user = message.user
    if not user:
        return
    unsubscribe_user_from_chat_updates(user, user.reply_channel)
