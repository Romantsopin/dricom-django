from django.contrib import admin
from .models import User, License, Token, Avatar
from django.contrib.auth.admin import UserAdmin

admin.site.register(User, UserAdmin)
admin.site.register(License)
admin.site.register(Token)
admin.site.register(Avatar)
