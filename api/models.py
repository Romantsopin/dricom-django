from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils import timezone

class User(AbstractUser):
    favorite_users = models.ManyToManyField(
        "self",
        related_name="users_added_me_to_favorite",
        symmetrical=False
    )

    def maybe_create_channel_with_user(self, rhs_user):
        channels = self.channel_set.all()
        for channel in channels:
            if channel.members.filter(id=rhs_user.id).exists():
                return channel

        channel = Channel()
        channel.save()
        self.channel_set.add(channel)
        rhs_user.channel_set.add(channel)
        self.save()
        rhs_user.save()
        return channel

    def send_message_to_channel(self, text, channel):
        message = Message.objects.create(
            text=text,
            owner=self,
            channel=channel
        )

    def send_message(self, text, to_user):
        channel = self.maybe_create_channel_with_user(to_user)
        message = Message.objects.create(
            text=text,
            owner=self,
            channel=channel
        )

    def __str__(self):
        return 'User<%s>' % self.username

class Channel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    members = models.ManyToManyField(User)

    @property
    def last_message(self):
        return self.message_set.first()


    def __str__(self):
        return 'Channel (members: %s)' % self.members

class Message(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)

    channel = models.ForeignKey(Channel)
    owner = models.ForeignKey(User)
    text = models.TextField()

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.channel.created_at = timezone.now()
        self.channel.save()

    class Meta:
        ordering = ['-created_at']


class Avatar(models.Model):
    image = models.ImageField(null=True, upload_to='avatars')
    user = models.OneToOneField(User)

    def __str__(self):
        return self.image.url

class License(models.Model):
    title = models.CharField(max_length=20, unique=True)
    user = models.ForeignKey(User, related_name='licenses')

    def __str__(self):
        return self.title

class Token(models.Model):
    value = models.CharField(max_length=100, unique=True)
    user = models.ForeignKey(User, related_name='tokens')

    def __str__(self):
        return self.value


class EmailAuthBackend(object):
    def authenticate(self, username=None, password=None):
        try:
            user = User.objects.get(email=username)
            if user.check_password(password):
                return user
            else:
                return None
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

class LicenseAuthBackend(object):
    def authenticate(self, username=None, password=None):
        try:
            user = User.objects.get(licenses__title=username)
            if user.check_password(password):
                return user
            else:
                return None
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
